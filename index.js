'use strict';
const
  express = require('express'),
  path = require('path'),
  favicon = require('serve-favicon'),
  logger = require('morgan'),
  cookieParser = require('cookie-parser'),
  bodyParser = require('body-parser'),
  app = express();

app.set('port', (process.env.PORT || 4000));

app.use(function (req, res, next) {
  res.set('location', 'https://www.kanjiflash.com');
  return res.status(301).send();
});

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(express.static(path.join(__dirname, 'public')));

// Redirect requests that aren't made to kanjiflash.com;
app.use(function (req, res, next) {
  if (app.get('env') === 'production') {
      if (req.hostname.toLowerCase() !== 'www.kanjiflash.com') {
        res.redirect(301, `http://www.kanjiflash.com${req.path}`);
      }

      next();
  }
});

app.get('*', function (req, res) {
  res.sendFile(path.join(__dirname, 'public/index.html'));
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

app.listen(app.get('port'), function() {
  console.log('KANJI FLASH SERVER is running on port', app.get('port'));
});
